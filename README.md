!---------------------README-------------------!
El presente proyecto SistemaAgricola modela un sistema compraventa entre agricultores y clientes.

Consideraciones de Uso
Al momento de crear un usuario y definir la direccion puede realizarlo mediante la direccion (Calle, Ciudad, Provincia, Pais)
o mediante las coordenadas latitud y longitud segun usted desee. Para el primer metodo se le mostrará las posibles ubicaciones
que el programa considera podria ser su ubicacion y debe seleccionar una de ellas para definir su ubicacion. El segundo metodo 
con las coordenadas define la direccion como la primera ocurrencia obtenida.

El filtro como nombre busca por coincidencias, es decir, si hay productos Zanahoria, Sandia, Naranja, Mandarina e ingresa "an"
le saldran todas ellas porque contienen la cadena "an" en ellas, por eso entre más especifico con el nombre mejor más particular
será el resultado.

El filtro categoria debe ser escrito de forma completa y elegir uno o varios de las categorias existentes separados por comas.
-CARNICOS
-VEGETALES
-FRUTAS
-LACTEOS
-CONSERVAS

El filtro de rango debe ser puesto como PrecioMin-PrecioMax sino su busqueda no mostrará resultados.

Los codigos de producto y pedido son automaticamente asignados por el sistema.

Si ingresa a una opcion por error y desea volver al menu principal, debe ingresar en el primer campo solicitado
un "-1" y el programa automaticamente redirige al menu anterior.

Credenciales Usuarios
Cliente 1
Usuario: jose123
Clave: 12345
Correo: jg7187671@gmail.com
Contraseña Correo: poo.12345

Cliente 2
Usuario: vigarome
Clave: 19990
Correo: garciaromva@gmail.com
Contraseña Correo:

Cliente 3
Usuario: remera
Clave: 12345

Proveedor 1
Usuario: jamguayo
Clave: 12345

Proveedor 2
Usuario: lissy
Clave: 12345