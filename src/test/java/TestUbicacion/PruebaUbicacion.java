/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestUbicacion;

import Sistema.Ubicacion;
import java.util.Scanner;

/**
 *
 * @author JonathanGarcia
 */
public class PruebaUbicacion {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String confirmacion = "";
        String direccion;
        Ubicacion ob = new Ubicacion();
        System.out.println("Ingrese Ubicacion separando por coma cada dato (Calle, Distrito, Ciudad, Pais)");
        
        do {
            direccion = sc.nextLine();
            ob.setDireccion(direccion);
            ob.mostrarResultados();
            System.out.println("¿Encuentra su ubicacion en los resultados? Y/N");
            confirmacion = sc.nextLine();
            if (confirmacion.equals("N")) {
                System.out.println("Ingrese la direccion con más detalles");
            }
        } while (confirmacion.equals("N"));
        
        
        
        System.out.println("Seleccion su localizacion (#)");
        int opcion = sc.nextInt();
        System.out.println(ob.obtenerCoordenada(opcion));
        
    }
}
