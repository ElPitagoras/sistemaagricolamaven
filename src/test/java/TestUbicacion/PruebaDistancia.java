/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestUbicacion;

import Sistema.Coordenada;
import Sistema.Ubicacion;
import usuario.Cliente;
import usuario.Proveedor;

/**
 *
 * @author JonathanGarcia
 */
public class PruebaDistancia {
    public static void main(String[] args) {
        Ubicacion ubic = new Ubicacion();
        //Cliente
        String direccion = "Paseo Shopping, Milagro, Guayas, Ecuador";
        ubic.setDireccion(direccion);
        Coordenada varCord = ubic.obtenerPrimeraCoordenadas();
        Cliente cliente1 = new Cliente("Jose Garcia", direccion, varCord, 
                                       "jose-1999@outlook.com", "jose123", "12345");
        direccion = "Virgen de Fatima, Guayas, Ecuador";
        ubic.setDireccion(direccion);
        varCord = ubic.obtenerPrimeraCoordenadas();
        Proveedor prov2 = new Proveedor("Lisset Flores", direccion, varCord,
                                        "lissflores@gmail.com", "MasterCard", "lissy", "12345");
        
        System.out.println("Distancia");
        System.out.println(cliente1.getUbicacion().toString());
        System.out.println(prov2.getUbicacion().toString());
        System.out.println(Coordenada.calcularDistancia(prov2.getUbicacion(), cliente1.getUbicacion()));
    }
    
}
