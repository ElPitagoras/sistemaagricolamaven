/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSistema;

import java.util.ArrayList;
import mercaderia.Categoria;
import mercaderia.Pedido;
import mercaderia.Producto;
import usuario.Cliente;
import Sistema.Coordenada;
import Sistema.MetodoPago;
import Sistema.PagoTarjeta;
import mercaderia.CopiaProducto;
import usuario.Proveedor;

/**
 *
 * @author Victor Andres Garcia Romero
 */
public class CambioEstadoPedido {
    public static void main(String[] args) {
    ArrayList<Categoria> cat;
    ArrayList<CopiaProducto> cps=new ArrayList();
    ArrayList<Producto> productos=new ArrayList();
    cat = new ArrayList();cat.add(Categoria.CARNICOS);
    Proveedor proveedor=new Proveedor("Pepito", "Eugenio Espejo", new Coordenada(-2.238279, -80.912824), "pepito@gmail.es", "0987654123", "pepito", "PEPEPEPE");
    Producto producto= new Producto("Chucusuela", "Es es la rodilla de la vaca", 1, cat);
    CopiaProducto cp=new CopiaProducto(producto, 1);
    productos.add(producto);
    cps.add(cp);
    Cliente cl = new Cliente("VicTor Garcia", "", new Coordenada(-2.237496,-80.913123), "garciaromva@gmail.com", "vigarome", "Andres1");
    MetodoPago pago=new PagoTarjeta(cl,"4932760092053376","Pacificard");
    cl.agregarMetodoPago(pago);
    Pedido pedido =new Pedido(cps, cl, pago, proveedor);
    proveedor.aceptarPedido(pedido);
    proveedor.registrarProducto(producto);
    proveedor.cambiaEstadoPedido("0001");
   cl.aceptarPedido(pedido);
    }
}
