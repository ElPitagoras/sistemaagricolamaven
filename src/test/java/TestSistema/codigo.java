/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSistema;

import Sistema.CarritoCompra;
import Sistema.Coordenada;
import Sistema.MetodoPago;
import Sistema.PagoTarjeta;
import Sistema.Ubicacion;
import usuario.Cliente;

/**
 *
 * @author Victor Andres Garcia Romero
 */
public class codigo {
    public static void main(String[] args) {
    Ubicacion ubic = new Ubicacion();
        //Cliente
        String direccion = "Paseo Shopping, Milagro, Guayas, Ecuador";
        ubic.setDireccion(direccion);
        Coordenada varCord = ubic.obtenerPrimeraCoordenadas();
        Cliente cliente1 = new Cliente("Jose Garcia", direccion, varCord, 
                                       "jose-1999@outlook.com", "jose123", "12345");
        MetodoPago pago=new PagoTarjeta(cliente1,"4932760092053376","Pacificard");
        PagoTarjeta pT=(PagoTarjeta)pago;
        cliente1.agregarMetodoPago(pago);
        CarritoCompra carrito=cliente1.getCarrito();
        System.out.println(pT.generarCodigo());
                          
}
}
