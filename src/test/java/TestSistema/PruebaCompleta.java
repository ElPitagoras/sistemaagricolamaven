/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSistema;

import Sistema.CarritoCompra;
import Sistema.Sistema;
import java.util.ArrayList;
import java.util.Scanner;
import mercaderia.Categoria;
import mercaderia.CopiaProducto;
import mercaderia.Producto;
import usuario.Cliente;
import usuario.Proveedor;
import usuario.Usuario;

/**
 *
 * @author JonathanGarcia
 */
public class PruebaCompleta {
    public static void main(String[] args) {
        Sistema sistRegistro = new Sistema();
        Scanner sc = new Scanner(System.in);
        //Miss revise con corazon de Madre
        int respuesta;
        
        do {
            String mensaje = "Bienvenido al Sistema Compra-Venta Agricola\n1. Iniciar Sesion\n"
                           + "2. Registrarse\n3. Salir";
            System.out.println(mensaje);
            respuesta = sistRegistro.esEntero();
            
            Usuario user = null;
            switch (respuesta) {
                case 1:
                    System.out.println("Ingrese su usuario:");
                    String usuario = sistRegistro.noVacio();
                    System.out.println("Ingrese su clave:");
                    String clave = sistRegistro.noVacio();
                    user = sistRegistro.hallarUsuario(usuario, clave);
                    if (user == null) {
                        System.out.println("Ingrese sus datos correctamente. "
                                         + "Volviendo a la pantalla de inicio.");
                    }
                    break;
                case 2:
                    System.out.println("Seleccione su rol\n1.Proveedor\n2.Cliente");
                    System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
                    int tipo = sistRegistro.esEntero();
                    do {
                        switch (tipo) {
                            case 1:
                                user = sistRegistro.solicitarDatosProveedor();
                                break;
                            case 2:
                                user = sistRegistro.solicitarDatosCliente();
                                break;
                            case -1:
                                System.out.println("Regresando al menu principal");
                                break;
                            default:
                                System.out.println("Ingrese una opcion valida");
                        }
                    } while (tipo < 1 || tipo > 2);
                    if (user != null) {
                        sistRegistro.registrar(user);
                        System.out.println("Registro exitoso");
                    }
                    break;
                case 3:
                    System.out.println("Saliendo del Programa");
                    break;
                default:
                    System.out.println("Ingrese valor correcto");
                    break;
            }
            
            Cliente cl = null;
            Proveedor pr = null;
            if (user instanceof Cliente) {
                cl = (Cliente) user;
                cl.getCarrito().realizarConsulta(sistRegistro.getListProveedores());
            } else if (user instanceof Proveedor) {
                pr = (Proveedor) user;
            }
            
            int opcion;
            
            if (pr != null) {
                do {
                    System.out.println("");
                    mensaje = "Hoy es un lindo dia. ¿Qué desea hacer?" + "\n1. Registrar Producto" +
                         "\n2. Consultar Pedidos" + "\n3. Cambiar Estado Pedido" + 
                         "\n4. Consultar Productos Registrados" + "\n5. Cerrar Sesion";
                    System.out.println(mensaje);
                    opcion = sistRegistro.esEntero();
                    switch (opcion) {
                        case 1:
                            Producto prod = sistRegistro.solicitarDatosProducto();
                            if (prod != null) {
                                pr.registrarProducto(prod);
                            }
                            break;
                        case 2:
                            pr.consultarPedidos();
                            break;
                        case 3:
                            System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
                            System.out.print("Ingrese el codigo del pedido a cambiar\n-");
                            String codigo = sistRegistro.noVacio();
                            if (!codigo.equals("-1")) {
                                pr.cambiaEstadoPedido(codigo);
                            } else {
                                System.out.println("Regresando al menu.");
                            }
                            break;
                        case 4:
                            System.out.println("Ingrese los siguientes datos");
                            System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
                            System.out.print("Nombre: ");
                            String nombre = sc.nextLine();
                            if (!nombre.equals("-1")) {
                                ArrayList<Categoria> categoria = sistRegistro.obtenerCategoria();
                                pr.consultarProducto(categoria, nombre);
                            } else {
                                System.out.println("Regresando al menu.");
                            }
                            break;
                        case 5:
                            System.out.println("Sesion Cerrada");
                            break;
                        default:
                            break;
                    }
                } while(opcion != 5);

            }

            if (cl != null) {
                CarritoCompra carrito = cl.getCarrito();
                carrito.realizarConsulta(sistRegistro.getListProveedores());
                do {
                    System.out.println("");
                    mensaje = "Hoy es un lindo dia. ¿Qué desea hacer?" + "\n1. Consultar Productos Cercano" +
                         "\n2. Consultar Carrito de Compras" + "\n3. Añadir Producto" + "\n4. Eliminar Producto" + 
                         "\n5. Confirmar Compra" + "\n6. Cerrar Sesion";
                    System.out.println(mensaje);
                    opcion = sistRegistro.esEntero();
                    switch (opcion) {
                        case 1:
                            System.out.println("Ingrese los siguientes datos\n"
                                    + "Si no desea filtrar por algun campo, deje vacio y aplaste Enter");
                            System.out.print("Si desea retroceder ingrese \"-1\" en este campo");
                            System.out.print("Nombre: ");
                            String nombre = sc.nextLine().toUpperCase();
                            if (!nombre.equals("-1")) {
                                ArrayList<Categoria> cat = sistRegistro.obtenerCategoria();
                                System.out.println("Rango de precio");
                                System.out.print("Precio Minimo: ");
                                double precioMin = sistRegistro.esDecimalOpcional();
                                System.out.println("Precio Maximo: ");
                                double precioMax = sistRegistro.esDecimalOpcional();
                                String rango = "" + precioMin + "-" + precioMax;
                                carrito.ConsultarProdCerca(cat, nombre, rango);
                            } else {
                                System.out.println("Regresando al menu");
                            }
                            break;
                        case 2:
                            carrito.consultarProds();
                            break;
                        case 3:
                            System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
                            System.out.print("Ingrese codigo del Producto\n-");
                            String añadir = sistRegistro.noVacio();
                            if (!añadir.equals("-1")) {
                                Producto prod = carrito.obtenerProducto(añadir);
                                if (prod != null) {
                                    System.out.print("¿Cuantas unidades desea?\n-");
                                    int cantidad = sistRegistro.esPositivo();
                                    carrito.añadirProducto(new CopiaProducto(prod, cantidad));
                                    System.out.println("Producto añadido exitosamente");
                                }
                            } else {
                                System.out.println("Regresando al menu");
                            }
                            break;
                        case 4:
                            System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
                            System.out.print("Ingrese codigo del Producto");
                            String eliminar = sistRegistro.noVacio();
                            if (!eliminar.equals("-1")) {
                                carrito.eliminarProd(eliminar);
                            } else {
                                System.out.println("Regresando al menu.");
                            }
                            break;
                        case 5:
                            if (carrito.getListProd().size() > 0) {
                                carrito.confirmarCompra();
                            } else {
                                System.out.println("No tiene productos en el carrito");
                            }
                            break;
                        case 6:
                            System.out.println("Sesion Cerrada");
                            break;
                        default:
                            System.out.println("Ingrese un campo valido");
                            break;
                    }
                } while(opcion != 6);
            }
        } while(respuesta != 3);
    }
}
