/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSistema;

import Sistema.Coordenada;
import Sistema.MetodoPago;
import Sistema.PagoTarjeta;
import java.util.ArrayList;
import mercaderia.Categoria;
import mercaderia.CopiaProducto;
import mercaderia.Pedido;
import mercaderia.Producto;
import usuario.Cliente;
import usuario.Proveedor;

/**
 *
 * @author JonathanGarcia
 */
public class PruebaProveedor {
    public static void main(String[] args) {
        ArrayList<Categoria> cat = new ArrayList();
        cat.add(Categoria.FRUTAS);
        Coordenada cod = new Coordenada(1,1);
        Proveedor prov = new Proveedor("Perez", "GYE", cod, "correo", "0939320", "Sujeto1", "12345");
        Producto prod1 = new Producto("Remolacha", "Verdura Proteica", 3.50, cat);
        Producto prod2 = new Producto("Vaina", "Verdura Simple", 5.50, cat);
        Producto prod3 = new Producto("Cebolla", "Tuberculo", 1, cat);

        CopiaProducto cProd1 = new CopiaProducto(prod1, 3);
        CopiaProducto cProd2 = new CopiaProducto(prod2, 5);
        CopiaProducto cProd3 = new CopiaProducto(prod3, 7);

        ArrayList<CopiaProducto> listaProd = new ArrayList();
        listaProd.add(cProd1);
        listaProd.add(cProd2);
        listaProd.add(cProd3);

        Cliente cl = new Cliente("VicTor Garcia", "", new Coordenada(-2.237496,-80.913123), "garciaromva@gmail.com", "vigarome", "Andres1");
        MetodoPago pago=new PagoTarjeta(cl,"4932760092053376","Pacificard");
        cl.agregarMetodoPago(pago);
        Pedido ped = new Pedido(listaProd, cl,pago, prov);
        prov.aceptarPedido(ped);
        prov.cambiaEstadoPedido("Ped-0001");
        prov.consultarPedidos();
        prov.registrarProducto(prod3);
        prov.editarProducto("0003", null, null, -1, null);
        prov.consultarProducto(null, "Cepa");
    }
}
