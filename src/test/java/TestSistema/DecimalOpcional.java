/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSistema;

import java.util.Scanner;

/**
 *
 * @author JonathanGarcia
 */
public class DecimalOpcional {
    Scanner sc;

    public DecimalOpcional() {
        this.sc = new Scanner(System.in);
    }
    
    public double esDecimalOpcional() {
        sc.nextLine();
        String input;
        input = sc.nextLine();
        while (!input.strip().equals("")) {
            if (!input.isBlank()) {
                Scanner sc2 = new Scanner(input);
                if(sc2.hasNextDouble()) {
                   return Double.parseDouble(input);
                } else {
                    System.out.println("Ingrese un enter o un decimal valido");
                    input = sc.nextLine();
                }
            }
        }
        return 0;
    }
    public static void main(String[] args) {
        DecimalOpcional decimal = new DecimalOpcional();
        System.out.println("Imprimir");
        double entrada = decimal.esDecimalOpcional();
        System.out.println(entrada);
    }
}
