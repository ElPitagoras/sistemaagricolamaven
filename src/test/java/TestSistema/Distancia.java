/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSistema;

import Sistema.Coordenada;
import Sistema.Sistema;
import java.util.ArrayList;
import usuario.Cliente;
import usuario.Proveedor;

/**
 *
 * @author Victor Andres Garcia Romero
 */
public class Distancia{
    public static void main(String [] args){
        Sistema sistem=new Sistema();
        ArrayList<Cliente> listcl=sistem.getListClientes();
        for (Cliente cl: listcl){
            for(Proveedor p: sistem.getListProveedores()){
                System.out.println("Cliente"+cl.getNombre()+"Proveedor"+p.getNombre());
                System.out.println("Ubicacion\n"+cl.getUbicacion()+""+p.getUbicacion()+"\n\n");
                if(Coordenada.calcularDistancia(cl.getUbicacion(), p.getUbicacion())<50){
                    System.out.println("SON CERCANOS\n");
                }
                
        }
        }
    }
}