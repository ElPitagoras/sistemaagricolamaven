/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestToString;

import Sistema.Coordenada;
import usuario.Cliente;

/**
 *
 * @author JonathanGarcia
 */
public class PruebaCliente {
    public static void main(String[] args) {
        Coordenada cod = new Coordenada(1,1);
        Cliente cliente = new Cliente("Juan", "Milagro", cod, "juan@espol.edu.ec", "Sujeto2", "12345");
        System.out.println(cliente);
    }
}
