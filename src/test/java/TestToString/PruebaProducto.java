/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestToString;

import Sistema.Coordenada;
import java.util.ArrayList;
import java.util.Date;
import mercaderia.Categoria;
import mercaderia.CopiaProducto;
import mercaderia.Producto;
import usuario.Proveedor;

/**
 *
 * @author JonathanGarcia
 */
public class PruebaProducto {
    public static void main(String[] args) {
        ArrayList<Categoria> cat = new ArrayList();
        cat.add(Categoria.FRUTAS);
        cat.add(Categoria.CONSERVAS);
        Coordenada cod = new Coordenada(1,1);
        Proveedor prov = new Proveedor("Perez", "GYE", cod, "correo", "0939320", "Sujeto2", "12345");
        Producto prod = new Producto("Remolacha", "Verdura Proteica", 3.50, cat);
        CopiaProducto cProd = new CopiaProducto(prod, 6);
        System.out.println(cProd.consultaProdCompra());
        System.out.println(prod.consultaProdUsuario());
        ArrayList<Categoria> cat1 = (ArrayList<Categoria>) cat.clone() ;
        System.out.println(cat.toString());
        cat1.remove(Categoria.LACTEOS);
        System.out.println(cat1.toString());
    }
}
