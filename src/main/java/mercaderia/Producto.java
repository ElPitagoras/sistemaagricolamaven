/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercaderia;

import Sistema.Sistema;
import java.util.ArrayList;
import usuario.Proveedor;

/**
 * Clase Producto
 * Es el producto con el precio unitario,
 * descripcion, categoria y proveedor del mismo
 * @author Dell
 */
public class Producto {
    private String codigo,nombre,descripcion;
    private double precioUnitario;
    private ArrayList <Categoria> categoria;
    private Proveedor proveedor;
    
    /**
     * Constructor
     * @param codigo
     * @param nombre
     * @param descripcion
     * @param precioUnitario
     * @param categoria 
     */
    public Producto(String nombre, String descripcion, double precioUnitario, ArrayList <Categoria> categoria) {
        this.codigo = Sistema.obtenerCodigoProducto();
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precioUnitario = precioUnitario;
        this.categoria = categoria;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public String getCodigo() {
        return codigo;
    }
    public String getNombre() {
        return nombre;
    }
    
    public void setNombre(String nombre){
        this.nombre=nombre;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion){
        this.descripcion=descripcion;
    }

    public ArrayList<Categoria> getCategoria() {
        return categoria;
    }
    public void setCategoria(ArrayList<Categoria> categoria){
        this.categoria=categoria;
    }

    public double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
    
    public void setPrecioUnitario(double precio){
        this.precioUnitario=precio;
    }
    
    /**
     * Este metodo va a retornar un String con 
     * la informacion del Producto
     * @return 
     */
    public String consultaProdUsuario() {
        String mensaje = String.format("%8s%15s%12.2f%25s", codigo, nombre, precioUnitario, categoria.toString());
        return mensaje;
    }
}
