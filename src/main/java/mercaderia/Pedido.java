/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercaderia;

import Sistema.MetodoPago;
import Sistema.PagoPaypal;
import Sistema.PagoTarjeta;
import Sistema.Sistema;
import java.util.ArrayList;
import java.util.Date;
import static mercaderia.EstadoPedido.Solicitado;
import usuario.Cliente;
import usuario.Proveedor;

/**
 * Clase que añade una lista de Productos 
 * que el Cliente desea 
 * @author JonathanGarcia
 */
public class Pedido {
    private String codigo;
    private Date fechaCompra;
    private ArrayList<CopiaProducto> listProd;
    private Cliente cliente;
    private MetodoPago datosPago;
    private double total;
    private Proveedor proveedor;
    private EstadoPedido estado;
    private Date fechaProcesando;
    private Date fechaDespachado;
    /**
     * Contructor1
     */
    public Pedido () {
        
    }
    /**
     * Constructor2
     * @param listProd
     * @param cliente
     * @param datosPago
     * @param proveedor 
     */
    public Pedido (ArrayList<CopiaProducto> listProd, Cliente cliente, MetodoPago datosPago, Proveedor proveedor) {
        this.codigo = Sistema.obtenerCodigoPedido();
        this.fechaCompra = new Date();
        this.listProd = listProd;
        this.cliente = cliente;
        this.datosPago = datosPago;
        this.proveedor = proveedor;
        this.total = obtenerTotal();
        this.estado = Solicitado;
    }
    
    /**
     * Este metodo va a obtener el total del pedido
     * @return 
     */
    private double obtenerTotal () {
        double suma = 0;
        for (CopiaProducto prod: listProd) {
            suma += prod.getCantidad() * prod.getProducto().getPrecioUnitario();
        }
        return suma;
    }
    
    @Override
    public String toString () {
        String mensaje = "!-----------------Pedido " + codigo + "-----------------!\n" + "Fecha: " + fechaCompra + "\nEstado: " + estado +
                  String.format("\n%30s\n", "Productos");
        String encabezado = String.format("%8s%15s%10s%12s%10s\n", "Codigo", "Nombre", "Cantidad", "PrecioUni", "Subtotal");
        String productos = "";
        for (CopiaProducto prod: listProd) {
            productos += prod.consultaProdCompra() + "\n";
        }
        String infoCliente = String.format("%35s\n", "Informacion Cliente") + cliente.toString();
        String costoTotal = "\nTotal Pedido: " + this.total;
        return mensaje + encabezado + productos + infoCliente + costoTotal;
    }
    
    /**
     * Retorna el estado actual del pedido
     * @return 
     */
    public EstadoPedido getEstado() {
        return estado;
    }
    
    /**
     * Actualiza el estado actual del pedido
     * @param estado 
     */
    public void setEstado(EstadoPedido estado) {
        this.estado = estado;
    }
    
    /**
     * Define la fecha de cambio de estado a Procesando del pedido
     * @param fechaProcesando 
     */
    public void setFechaProcesando(Date fechaProcesando) {
        this.fechaProcesando = fechaProcesando;
    }
    
    /**
     * Define la fecha de cambio de estado a Despachado del pedido.
     * @param fechaDespachado 
     */
    public void setFechaDespachado(Date fechaDespachado) {
        this.fechaDespachado = fechaDespachado;
    }
    
    /**
     * Retorna el codigo del pedido
     * @return 
     */
    public String getCodigo() {
        return codigo;
    }
    
    /**
     * Retorna el cliente al que hace referencia el pedido
     * @return 
     */
    public Cliente getCliente() {
        return cliente;
    }

    /**
     * Metodo que retorna la 
     * informacion del metodo de pago
     * @return 
     */
    public String getDatosPago() {
        if(this.datosPago instanceof PagoTarjeta){
            PagoTarjeta pT= (PagoTarjeta)this.datosPago;
            return "Tarjeta de Credito"+pT.toString();
        }else if(this.datosPago instanceof PagoPaypal){
            return "PayPal";
        }
        return null;
    }
    
    /**
     * Retorna el proveedor al que hace referencia el pedido
     * @return 
     */
    public Proveedor getProveedor() {
        return proveedor;
    }
}
