/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercaderia;

/**
 *
 * @author JonathanGarcia
 */
public class CopiaProducto {
    private Producto producto;
    private int cantidad;
    
    /**
     * Constructor1
     * @param producto
     * @param cantidad 
     */
    public CopiaProducto (Producto producto, int cantidad) {
        this.producto = producto;
        this.cantidad = cantidad;
    }
    
    /**
     * Este metodo va a retornar un String
     * con la informacion del Producto que se copia
     * @return 
     */
    public String consultaProdCompra() {
        String mensaje = String.format("%8s%15s%10d%12.2f%10.2f", producto.getCodigo(), producto.getNombre(), 
                                        cantidad, producto.getPrecioUnitario(), 
                                        cantidad*producto.getPrecioUnitario());
        return mensaje;
    }
    
    /**
     * Aumenta la cantidad de unidades un producto
     * @param cantidad 
     */
    public void sumarExistencia(int cantidad) {
        this.cantidad += cantidad;
    }
    
    /**
     * Retorna el objeto producto al que hace referencia
     * @return 
     */
    public Producto getProducto() {
        return producto;
    }
    
    /**
     * Retorna la cantidad de unidades del producto
     * @return 
     */
    public int getCantidad() {
        return cantidad;
    }
    
    
}
