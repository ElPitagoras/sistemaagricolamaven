/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mercaderia;

/**
 *
 * @author Victor Andres Garcia Romero
 */
public enum EstadoPedido {
    Solicitado,
    Procesando,
    Despachado
}
