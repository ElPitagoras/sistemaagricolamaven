/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import java.util.Scanner;
import usuario.Cliente;

/**
 *La clase que hereda de Metodo de pago
 * @author Victor Garcia
 */
public class PagoTarjeta extends MetodoPago{
    private Cliente cliente;
    private String codigo;
    private String tipo;
    private String titular;
    /**
     * Constructor
     * @param cliente
     * @param codigo
     * @param tipo
     */
    public PagoTarjeta (Cliente cliente, String codigo, String tipo) {
        this.cliente = cliente;
        this.codigo = codigo;
        this.tipo = tipo;
        this.titular = cliente.getNombre();   
    }
    
    /**
     * El código tiene que tener 5 caracteres,
     * 3 números 
     * 2 letras
     * El número aleatorio se coloca es un número unitario, si el nunero aleatorio sale mayor a 9 se pone una letra. 
     * El índice para tiene que tener un número par o tiene q tener una letra del alfabeto con índice par, y así mismo con el impar
     * @return 
     */
    public String generarCodigo(){
        String codigoVerificacion = "";
        String[] alfabeto={"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P"
                + "Q","R","S","T","U","V","W","X","Y","Z"};
        int conf=0;
        int alf=0;
        int num=0;
        while(conf<5){
            int nAlet=(int) (Math.random()*26);
            System.out.println(nAlet);
            if ((nAlet%2==0 && conf%2==0)||(nAlet%2==1 && conf%2==1)){
                if(num<3 && nAlet<=9){
                  codigoVerificacion+=String.valueOf(nAlet);
                  conf+=1;num+=1;  
                }else if(alf<2||(alf<2&&num==2)){
                  codigoVerificacion+=alfabeto[nAlet];
                  conf+=1;alf+=1;
                } 
            }
        }
        return codigoVerificacion;
    }
    
    @Override
    public boolean procesarPago(double precio) {
        String conf;
        String codigoConf = this.generarCodigo();
        Mail correo = new Mail();
        correo.sendMail(cliente.getCorreo(), "CONFIRMACION DE TARJETA DE CREDITO", codigoConf);
        System.out.println("Se envio un correo con el codigo de confirmacion");
        Scanner sc = new Scanner(System.in);
        do{
            System.out.println("Ingrese el codigo ");
            conf = sc.nextLine();
        }while(!conf.equals(codigoConf));
        return false;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "codigo=" + codigo + ", tipo=" + tipo ;
    }
    
}
