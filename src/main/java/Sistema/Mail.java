/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *Clase donde va permitir enviar correos
 * @author Victor Andres Garcia Romero
 */
public class Mail {

    /**
     * @param args the command line arguments
     */
    private final String remitente="sistemagricola5@gmail.com";
    private final String contra="ProgramacionPoo";
    /**
     * Metodo donde va a enviar un correo
     * al Cliente
     * @param destino->correo del Cliente
     * @param asunto
     * @param msj 
     */
    public void sendMail(String destino,String asunto,String msj){
        Properties props=new Properties();
        props.put("mail.smtp.host","smtp.gmail.com");
        props.put("mail.smtp.port","587");
        props.put("mail.smtp","true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.user",this.remitente);
        props.put("mail.smtp.clave",this.contra);
        
        Session session=Session.getDefaultInstance(props);
        MimeMessage mensaje=new MimeMessage(session);
        
        try {
            mensaje.addRecipient(Message.RecipientType.TO, new InternetAddress(destino));
            mensaje.setSubject(asunto);
            mensaje.setText(msj);
            Transport transport= session.getTransport("smtp");
            transport.connect("smtp.gmail.com",this.remitente,this.contra);
            transport.sendMessage(mensaje, mensaje.getAllRecipients());
            transport.close();
            System.out.println("Correo enviado");
        } catch (Exception e){
            e.printStackTrace();
        }
                
    }
}
