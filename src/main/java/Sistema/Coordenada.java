/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

/**
 *  Clase donde se va a obtener la Coordenada(gps)
 * mediante su lat,lng
 * @author JonathanGarcia
 */
public class Coordenada {
    private double lat;
    private double lng;
    final static double RADIOTIERRA=6378.137;
    
    /**
     * Constructor
     * @param lat->latitud
     * @param lng ->longitud
     */
    public Coordenada (double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    /**
     * Metodo que permite calcular la distancia de 2 puntos(coordenada)
     * @param c1->Coordenada1
     * @param c2->Coordenada2
     * @return 
     */
    public static double calcularDistancia(Coordenada c1,Coordenada c2){
        double dlat, dlong, a, c, dist;
        dlat=c2.lat - c1.lat;
        dlong=c2.lng - c1.lng;
        a = Math.pow(Math.sin(Math.toRadians(dlat)/2), 2) + Math.cos(Math.toRadians(c1.lat)) * 
            Math.cos(Math.toRadians(c2.lat)) * Math.pow(Math.sin(Math.toRadians(dlong)/2), 2);
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        dist = c * RADIOTIERRA;
        return dist;
    }
    
    @Override
    public String toString() {
        return "Coordenada{" + "lat=" + lat + ", lng=" + lng + '}';
    }
}
