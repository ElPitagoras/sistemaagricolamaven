/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import com.byteowls.jopencage.JOpenCageGeocoder;
import com.byteowls.jopencage.model.JOpenCageComponents;
import com.byteowls.jopencage.model.JOpenCageForwardRequest;
import com.byteowls.jopencage.model.JOpenCageLatLng;
import com.byteowls.jopencage.model.JOpenCageResponse;
import com.byteowls.jopencage.model.JOpenCageResult;
import com.byteowls.jopencage.model.JOpenCageReverseRequest;
import java.util.List;

/**
 *
 * @author JonathanGarcia
 */
public class Ubicacion {
    private String direccion;
    private List<JOpenCageResult> listLocalizacion;
    JOpenCageGeocoder jOpenCageGeocoder = new JOpenCageGeocoder("b9d027b3cac7485fa8e9ff9d18aa7d76");
    /**
     * Constructor1
     */
    public Ubicacion () {
        
    }
    /**
     * Constructor2
     * Con la direccion pasada por parametro va a buscarla en gps
     * Y va a obtener sus coordenadas
     * @param direccion 
     */
    public Ubicacion (String direccion) {
        this.direccion = direccion;
        this.listLocalizacion = this.obtenerPosiblesSitios();
    }
    
    /**
     * Este metodo permite obtener la direcion
     * mediantes los valores de las coordenadas(lat,lng)
     * @param lat-> latitud
     * @param lng->longitud
     * @return 
     */
    public String obtenerDireccion(double lat, double lng) {
        JOpenCageReverseRequest request = new JOpenCageReverseRequest(lat, lng);
        request.setLanguage("es");
        request.setNoDedupe(true); // don't return duplicate results
        request.setLimit(5); // only return the first 5 results (default is 10)
        request.setNoAnnotations(true); // exclude additional info such as calling code, timezone, and currency
        request.setMinConfidence(3); // restrict to results with a confidence rating of at least 3 (out of 10)

        JOpenCageResponse response = jOpenCageGeocoder.reverse(request);

        // get the formatted address of the first result:
        String formattedAddress = response.getResults().get(0).getFormatted(); 
        // formattedAddress is now 'Travessera de Gràcia, 142, 08012 Barcelona, España'
        return formattedAddress;
    }
    
    /**
     * Este metodo permite obtener
     * una lista con los posibles sitios
     * @return 
     */
    private List<JOpenCageResult> obtenerPosiblesSitios() {
        JOpenCageForwardRequest request = new JOpenCageForwardRequest(direccion);
        //request.setRestrictToCountryCode("za"); // restrict results to a specific country
        request.setLanguage("es");
        request.setLimit(5);
        
        JOpenCageResponse response = jOpenCageGeocoder.forward(request); //Pide respuesta
        return response.getResults();
    }
    
    /**
     * Recorre la lista de los sitios para mostrar sus datos
     */
    public void mostrarResultados () {
        int i = 1;
        for (JOpenCageResult result: listLocalizacion) {
            System.out.println("Localizacion #" + i);
            presentarResultado(result);
            i++;
        }
    }
    
    /**
     * Este metodo obtiene la Primera ocurrencia de Coordenada
     * de la direcion
     * @return 
     */
    public Coordenada obtenerPrimeraCoordenadas () {
         JOpenCageForwardRequest request = new JOpenCageForwardRequest(direccion);
        //request.setRestrictToCountryCode("za"); // restrict results to a specific country
        request.setLanguage("es");
        request.setLimit(5);
        
        JOpenCageResponse response = jOpenCageGeocoder.forward(request); //Pide respuesta
        double lat = response.getFirstPosition().getLat();
        double lng = response.getFirstPosition().getLng();
        return new Coordenada(lat, lng);
    }
    
    /**
     * Método que presentará la ubicacion encontrada con formato agradable a la vista.
     * @param resultado 
     */
    private void presentarResultado(JOpenCageResult resultado) {
        JOpenCageComponents info = resultado.getComponents();
        String ISO = info.getIso31661Alpha3();
        String sitio = info.getCounty();
        String provincia = info.getState();
        String pais = info.getCountry();
        String tipo = info.getType();
        JOpenCageLatLng posicion = resultado.getGeometry();
        double Lat = posicion.getLat();
        double Lng = posicion.getLng();
        
        String mensaje = "Sitio: " + ISO + ", " + sitio + ", " + provincia + ", " + pais + "\nTipo: " + tipo + "\n(" + Lat + ", " + Lng + ")\n";
        
        System.out.println(mensaje);
    }
    
    /**
     * Obtiene las coordenadas de un sitio
     * @param opcion
     * @return 
     */
    public Coordenada obtenerCoordenada(int opcion) {
        JOpenCageResult seleccion =  listLocalizacion.get(opcion);
        System.out.println("Resultado añadido con exito");
        return new Coordenada(seleccion.getGeometry().getLat(), seleccion.getGeometry().getLng());
    }
    
    /**
     * Retorna la direccion actual en la instancia
     * @return 
     */
    public String getDireccion() {
        return direccion;
    }
    
    /**
     * Define una nueva direccion y automaticamente busca nuevos sitios
     * @param direccion 
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
        this.listLocalizacion = this.obtenerPosiblesSitios();
    }
}
