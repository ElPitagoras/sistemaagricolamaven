/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

/**
 * Clase que hereda de Metodo de Pago
 * tiene un valor de 100$ hasta 900$
 * que es dinero que tiene en la cuenta
 * @author JonathanGarcia
 */
public class PagoPaypal extends MetodoPago{
    double saldo;
    /**
     * Constructor
     * Asigno al objeto actual un valor aleatorio entre 100 y 1000
     */
    public PagoPaypal () {
        this.saldo = (Math.random() * 900) + 100;
    }
    
    @Override
    public boolean procesarPago(double precio) {
        if (saldo <= precio) {
            return false;
        }
        return true;
    }
    
}
