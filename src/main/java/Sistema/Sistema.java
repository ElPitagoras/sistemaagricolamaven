/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import java.util.ArrayList;
import java.util.Scanner;
import mercaderia.Categoria;
import mercaderia.Producto;
import usuario.Cliente;
import usuario.Proveedor;
import usuario.Usuario;

/**
 *
 * @author JonathanGarcia
 */
public class Sistema {
    Scanner sc = new Scanner(System.in);
    private static int ultimoCodigoPedido;
    private static int ultimoCodigoProducto;
    private ArrayList<Cliente> listClientes;
    private ArrayList<Proveedor> listProveedores;
    private boolean continuar;
    /**
     * Constructor
     * Inicializa las listas para su correcto uso
     * Crea clientes y proveedores en el sistema
     */
    public Sistema() {
        listClientes = new ArrayList();
        listProveedores = new ArrayList();
        inicializarSistema();
    }
    
    /**
     * Crea clientes y proveedores en el sistema
     */
    private void inicializarSistema() {
        Ubicacion ubic = new Ubicacion();
        //Cliente
        String direccion = "Paseo Shopping, Milagro, Guayas, Ecuador";
        ubic.setDireccion(direccion);
        Coordenada varCord = ubic.obtenerPrimeraCoordenadas();
        Cliente cliente1 = new Cliente("Jose Garcia", direccion, varCord, 
                                       "jg7187671@gmail.com", "jose123", "12345");
        Cliente cliente2 = new Cliente("Victor Garcia", direccion, varCord,
                                        "garciaromva@gmail.com", "vigarome", "19990");
        direccion = "Malecon 2000, Guayaquil, Guayas, Ecuador";
        ubic.setDireccion(direccion);
        Cliente cliente3 = new Cliente("Rocio Mera", direccion,  varCord,
                                        "remera@espol.edu.ec", "remera", "12345");
        MetodoPago pago1 = new PagoPaypal();
        MetodoPago pago2 = new PagoTarjeta(cliente2, "4932760092053376", "Pacificard");
        MetodoPago pago3 = new PagoTarjeta(cliente3, "4932765892053378", "MASTERCARD");
        cliente1.agregarMetodoPago(pago1);
        cliente2.agregarMetodoPago(pago2);
        cliente3.agregarMetodoPago(pago3);
        listClientes.add(cliente3);
        listClientes.add(cliente2);
        listClientes.add(cliente1);
        
        //Productos
        ArrayList<Categoria> cat;
        cat = new ArrayList();cat.add(Categoria.FRUTAS);
        Producto prod1 = new Producto("Sandía", "Deliciosa Fruta de los mejores sembrios", 3.50, cat);
        cat = new ArrayList();cat.add(Categoria.VEGETALES);
        Producto prod2 = new Producto("Zanahoria", "Verdura Proteica", 5.50, cat);
        cat = new ArrayList();cat.add(Categoria.VEGETALES);
        Producto prod3 = new Producto("Cebolla", "Tuberculo de primera calidad", 1, cat);
        cat = new ArrayList();cat.add(Categoria.LACTEOS);cat.add(Categoria.CONSERVAS);
        Producto prod4 = new Producto("Mantequilla", "Soy una descripcion", 3, cat);
        cat = new ArrayList();cat.add(Categoria.CARNICOS);
        Producto prod5 = new Producto("Atun", "Inserte descripcion referente", 4, cat);
        cat = new ArrayList();cat.add(Categoria.CONSERVAS);
        Producto prod6 = new Producto("Cerveza", "La mejor fermentacion de la ciudad", 6, cat);
        
        //Proveedores
        direccion = "Naranjito, Guayas Ecuador";
        ubic.setDireccion(direccion);
        varCord = ubic.obtenerPrimeraCoordenadas();
        Proveedor prov1 = new Proveedor("Jaime Aguayo",direccion, varCord, 
                                        "jaime-1998@hotmail.com", "096019214", "jamguayo", "12345");
        prov1.registrarProducto(prod1);
        prov1.registrarProducto(prod2);
        prov1.registrarProducto(prod3);
        
        direccion = "Virgen de Fatima, Guayas, Ecuador";
        ubic.setDireccion(direccion);
        varCord = ubic.obtenerPrimeraCoordenadas();
        Proveedor prov2 = new Proveedor("Lisset Flores", direccion, varCord,
                                        "lissflores@gmail.com", "0968742781", "lissy", "12345");
        prov2.registrarProducto(prod4);
        prov2.registrarProducto(prod5);
        prov2.registrarProducto(prod6);
        
        this.listProveedores.add(prov1);
        this.listProveedores.add(prov2);
    }
    
    /**
     * Retorna la instancia usuario en caso de hallar una
     * @param nombreUsuario -> Nombre unico de usuario
     * @param clave -> Contraseña del usuario
     * @return 
     */
    public Usuario hallarUsuario(String nombreUsuario, String clave) {
        for (Cliente cl: this.listClientes) {
            if (cl.getNombreUsuario().equals(nombreUsuario) && cl.getClave().equals(clave)) {
                return cl;
            }
        }
        for (Proveedor prov: this.listProveedores) {
            if (prov.getNombreUsuario().equals(nombreUsuario) && prov.getClave().equals(clave)) {
                return prov;
            }
        }
        return null;
    }
    
    /**
     * Registra un usuario
     * @param user 
     */
    public void registrar(Usuario user) {
        if (user instanceof Cliente) {
            Cliente cl = (Cliente) user;
            listClientes.add(cl);
        } else if (user instanceof Proveedor) {
            Proveedor pr = (Proveedor) user;
            listProveedores.add(pr);
        }
    }
    
    /**
     * Retorna verdadero si un nombre de usuario esta disponible en el sistema
     * @param nombreUsuario -> Nombre temporal de usuario no registrado aun
     * @return 
     */
    public boolean verificarDisponibilidad(String nombreUsuario) {
        for (Proveedor prov: this.listProveedores) {
            if (prov.getNombreUsuario().equals(nombreUsuario)) {
                return false;
            }
        }
        for (Cliente client: this.listClientes) {
            if (client.getNombreUsuario().equals(nombreUsuario)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Solicita los datos en comun tanto para cliente como proveedor
     * @return 
     */
    public Usuario solicitarDatosUsuario() {
        continuar = true;
        System.out.println("Ingrese los datos");
        System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
        System.out.print("Nombre: ");
        String nombre = noVacio();
        
        if (!nombre.equals("-1")) {
            System.out.println("Obtener Ubicacion por\n1. Direccion\n2. Coordenada");
            int respuesta = esEntero();

            Coordenada ubicacion = null;
            String direccion = null;
            Ubicacion ubic = new Ubicacion();
            do {
                switch (respuesta) {
                    case 1:
                        String confirmacion;
                        System.out.print("Ingrese Ubicacion separando por coma cada dato (Calle, Distrito, Ciudad, Pais)\n-");
                        do {
                            direccion = noVacio();
                            ubic.setDireccion(direccion);
                            ubic.mostrarResultados();
                            System.out.print("¿Encuentra su ubicacion en los resultados? Y/N\n-");
                            confirmacion = sc.nextLine();
                            if (confirmacion.equals("N")) {
                                System.out.print("Ingrese la dirección con más detalles\n-");
                            }
                        } while (confirmacion.equals("N"));
                        System.out.print("Seleccion su localizacion (#)\n-");
                        int opcion = esEntero();
                        ubicacion = ubic.obtenerCoordenada(opcion);
                        break;
                    case 2:
                        System.out.println("Ingrese sus Coordenadas (Ej. lat, lng");
                        System.out.println("Ingrese la latitud");
                        double lat = esDecimal();
                        System.out.println("Ingrese la longitud");
                        double lng = esDecimal();

                        ubicacion = new Coordenada(lat, lng);
                        direccion = ubic.obtenerDireccion(lat, lng);
                        break;
                    default:
                        System.out.println("Ingrese una opcion valida");
                        break;
                }
            } while (respuesta < 1 || respuesta > 2);


            System.out.print("Correo: ");
            String correo = noVacio();

            System.out.print("Nombre de Usuario: ");
            String nombreUsuario;
            do {
                nombreUsuario = noVacio();
                if (!verificarDisponibilidad(nombreUsuario)) {
                    System.out.println("Nombre usuario no disponible");
                    System.out.print("Ingrese otro Nombre de Usuario\n-");
                }
            } while (!verificarDisponibilidad(nombreUsuario));

            System.out.print("Clave: ");
            String clave = noVacio();

            return new Usuario(nombre, direccion, ubicacion, correo, nombreUsuario, clave);
        }
        return null;
    }
    
    /**
     * Solicita los datos especificos del proveedor
     * @return 
     */
    public Proveedor solicitarDatosProveedor() {
        Usuario usuario = solicitarDatosUsuario();
        if (usuario != null) {
            System.out.print("Numero de Contacto: ");
            String contacto = noVacio();
            return new Proveedor(usuario, contacto);
        }
        return null;
    }
    
    /**
     * Solicita los datos especificos del cliente
     * @return 
     */
    public Cliente solicitarDatosCliente() {
        Usuario usuario = solicitarDatosUsuario();
        if (usuario != null) {
            Cliente cl = new Cliente(usuario);
            MetodoPago metodoPago = null;
            System.out.print("Escoja su metodo de pago: ");
            System.out.println("1. Tarjeta de Credito");
            System.out.println("2. Paypal");
            int pago;
            do{
                pago = esEntero();
                switch(pago){
                    case 1:
                        String tipo, numero;
                        System.out.println("**********INGRESE DATOS DE TAJETA DE CREDITO**********");
                        System.out.println("Tipo de Tarjeta: ");
                        tipo = noVacio();
                        System.out.println("Tipo el numero: ");
                        numero = noVacio(); 
                        while(numero.length() != 16) {
                            System.out.println("Campo ingresado invalido. Ingrese nuevamente");
                            numero = noVacio(); 
                        }
                        cl.agregarMetodoPago(new PagoTarjeta(cl, numero, tipo));
                        return cl;
                    case 2:
                        cl.agregarMetodoPago(new PagoPaypal());
                        break;
                    default:
                        System.out.println("Ingrese un campo valido");
                }
            } while(pago < 1 || pago > 2);
            cl.agregarMetodoPago(metodoPago);
            return cl;
        }
        return null;
    }
    
    /**
     * Retorna un codigo unico autoincremental para los pedidos
     * @return 
     */
    public static String obtenerCodigoPedido() {
        ultimoCodigoPedido += 1;
        String codigo = String.format("%04d", ultimoCodigoPedido);
        return codigo;
    }
    
    /**
     * Retorna un codigo unico autoincremental para los productos
     * @return 
     */
    public static String obtenerCodigoProducto() {
        ultimoCodigoProducto += 1;
        String codigo = String.format("%04d", ultimoCodigoProducto);
        return codigo;
    }
    
    /**
     * Solicita los datos para registrar un producto
     * @return 
     */
    public Producto solicitarDatosProducto() {
        System.out.println("Ingrese los siguientes datos");
        continuar = true;
        System.out.println("Si desea retroceder ingrese \"-1\" en este campo");
        System.out.print("Nombre: ");
        String nombre = noVacio();
        if (nombre.equals("-1")) {
            continuar = false;
        }
        if (continuar) {
            System.out.print("Descripcion: ");
            String descripcion = noVacio();

            System.out.print("Precio Unitario: ");
            double precio = esDecimal();

            ArrayList<Categoria> cat = obtenerCategoria();
            return new Producto(nombre, descripcion, precio, cat);
        } else {
            System.out.println("Regresando al menu.");
            return null;
        }
        
    }
    
    /**
     * Solicita en especifico las categorias de un producto a registrar o buscar
     * Crea un ArrayList de ellos para el uso en el programa
     * @return 
     */
    public ArrayList<Categoria> obtenerCategoria() {
        System.out.print("Categoria(s) a la que pertenece (Separado por coma)\n-");
        String[] categorias = sc.nextLine().toUpperCase().replace(" ", "").split(",");
        if (categorias.length > 0) {
            if (!categorias[0].equals("")) {
                ArrayList<Categoria> cat = new ArrayList();
                for (String st: categorias) {
                    switch (st) {
                        case "FRUTAS":
                            cat.add(Categoria.FRUTAS);
                            break;
                        case "CARNICOS":
                            cat.add(Categoria.CARNICOS);
                            break;
                        case "LACTEOS":
                            cat.add(Categoria.LACTEOS);
                            break;
                        case "CONSERVAS":
                            cat.add(Categoria.CONSERVAS);
                            break;
                        case "VEGETALES":
                            cat.add(Categoria.VEGETALES);
                            break;
                        default:
                            System.out.println("Su Categoria " + st + "no existe y no se la incluirá");
                            break;
                    }
                }
                return cat;
            }
        }
        return null;
    }
    
    /**
     * Devuelve un ArrayList de los proveedores
     * @return 
     */
    public ArrayList<Proveedor> getListProveedores() {
        return listProveedores;
    }
    
    /**
     * Devuelve un ArrayList de los clientes
     * @return 
     */
    public ArrayList<Cliente> getListClientes() {
        return listClientes;
    }
    
    //Validaciones
    /**
     * Valida entradas obligatorias
     * @return 
     */
    public String noVacio() {
        String input = sc.nextLine();
        while (input.isBlank()) {
            System.out.println("Ingrese un campo valido");
            input = sc.nextLine();
        }
        return input;
    }
    
    /**
     * Valida entradas obligatorias de enteros
     * @return 
     */
    public int esEntero() {
        String input = noVacio();
        Scanner sc2 = new Scanner(input);
        while (!sc2.hasNextInt()) {
            System.out.print("Ingrese un entero\n-");
            input = noVacio();
            sc2 = new Scanner(input);
        }
        return Integer.parseInt(input);
    }
    
    /**
     * Valida entradas de numero decimales o enteros (se convertiran en double) obligatorias
     * @return 
     */
    public double esDecimal() {
        String input = noVacio();
        Scanner sc2 = new Scanner(input);
        while (!sc2.hasNextDouble()) {
            System.out.print("Ingrese un campo valido\n-");
            input = noVacio();
            sc2 = new Scanner(input);
        }
        return Double.parseDouble(input);
    }
    
    /**
     * Valida una entrada decimal o vacia
     * @return 
     */
    public double esDecimalOpcional() {
        String input;
        input = sc.nextLine();
        while (!input.strip().equals("")) {
            if (!input.isBlank()) {
                Scanner sc2 = new Scanner(input);
                if(sc2.hasNextDouble()) {
                   return Double.parseDouble(input);
                } else {
                    System.out.println("Ingrese un enter o un decimal valido");
                    input = sc.nextLine();
                }
            }
        }
        return 0;
    }
    
    /**
     * Valida entradas de enteros positivas obligatorias
     * @return 
     */
    public int esPositivo() {
        int input = esEntero();
        while (input <= 0) {
            System.out.println("Ingrese un numero Positivo");
            input = esEntero();
        }
        return input;
    }
}
