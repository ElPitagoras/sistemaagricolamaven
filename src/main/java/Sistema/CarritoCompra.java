/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sistema;

import java.util.ArrayList;
import mercaderia.Categoria;
import mercaderia.CopiaProducto;
import mercaderia.Pedido;
import mercaderia.Producto;
import usuario.Cliente;
import usuario.Proveedor;

/**
 *Clase donde se va a guardar los productos que el cliente seleciona en el pedido,
 * puede modificar el pedido
 * @author JonathanGarcia
 */
public class CarritoCompra {
    private ArrayList<CopiaProducto> listProd;
    private ArrayList<Proveedor> listProv;
    private double total;
    private final Cliente cliente;
    private ArrayList<Producto> listProdCoincd;
    private ArrayList<Proveedor> listProvCoincd;
    private Coordenada ubicCliente;
    private MetodoPago pago;
    
    /**
     * Constructor
     * @param cliente 
     */
    public CarritoCompra (Cliente cliente) {
        this.cliente = cliente;
        this.ubicCliente = cliente.getUbicacion();
        this.listProd = new ArrayList();
        this.listProv = new ArrayList();
    }
    
    /**
     * Metodo donde inicia los metodos
     * obtenerProvCerca()
     * obtenerProdCerca
     * @param proveedores -> Lista de proveedores registrados en el sistema
     */
    public void realizarConsulta(ArrayList<Proveedor> proveedores) {
        this.listProvCoincd = obtenerProvCerca(proveedores);
        this.listProdCoincd = obtenerProdCerca();
    }
    
    /**
     * Metodo que obtiene los proveedores a una distancia de 50km a la redonda
     * @param proveedores -> Lista de proveedores registrados en el sistema
     * @return -> Lista de Proveedores cerca
     */
    private ArrayList<Proveedor> obtenerProvCerca(ArrayList<Proveedor> proveedores){
        ArrayList<Proveedor> listProvCerca = new ArrayList();
        for(Proveedor p : proveedores){
            Coordenada ubiP = p.getUbicacion();
            double dist = Coordenada.calcularDistancia(ubicCliente, ubiP);
            if (dist<=50){
                listProvCerca.add(p);
            }
        }
        return listProvCerca;
    }
    
    /**
     * Metodo que obtiene los productos de los Productos
     * de los Proveedores cerca
     * @return -> Lista de los productos cerca
     */
    private ArrayList<Producto> obtenerProdCerca() {
        ArrayList<Producto> listProdCerca = new ArrayList();
        for (Proveedor prov: this.listProvCoincd) {
            for(Producto p: prov.getListProducto()) {
                listProdCerca.add(p);
            }
        }
        return listProdCerca;
    }
    
    /**
     * Metodo que presenta por pantalla los productos
     * que coincide con los filtros pasados como @param
     * (los parametros pueden ser vacios)
     * @param categoria -> 0 a 5 Categorias de los productos
     * @param nombre -> Completo o parcial
     * @param rango -> rango de precio separado por "-"
     */
    public void ConsultarProdCerca(ArrayList<Categoria> categoria, String nombre, String rango){
        ArrayList<Producto> listProdMatched = (ArrayList<Producto>) this.listProdCoincd.clone();
        if (categoria != null) {
            for (Producto prod: listProdMatched) {
                if (!categoria.containsAll(prod.getCategoria())) {
                    listProdMatched.remove(prod);
                }
            }
        }
        if (!nombre.isBlank()) {
            for (Producto prod: this.listProdCoincd) {
                if (!(prod.getNombre()).contains(nombre)){
                    listProdMatched.remove(prod);
                }
            }
        }
        if (!rango.isBlank()) {
            if (rango.split("-").length == 2) {
                double precioMin=Double.parseDouble(rango.split("-")[0]);
                double precioMax=Double.parseDouble(rango.split("-")[1]);
                if (precioMin+precioMax > 0) {
                    for (Producto prod: listProdMatched) {
                        if (!(prod.getPrecioUnitario()>=precioMin && prod.getPrecioUnitario()<=precioMax)) {
                            listProdMatched.remove(prod);
                        }
                    }
                }
            }
        }
        
        for (Producto prod: listProdCoincd) {
            System.out.println(prod.consultaProdUsuario());
        }
    }
    
    public Producto obtenerProducto(String codigoProd){
        for (Producto prod: listProdCoincd){
            if (prod.getCodigo().equals(codigoProd)){
                return prod;
            }
        }
        System.out.println("Codigo no existe");
        return null;
    }
    
    /**
     * Metodo que envia un correo al cliente 
     * con la informacion de su pedido
     */
    public void confirmarCompra () {
        this.pago = cliente.getDatosPago().get(0);
        if (pago.procesarPago(calcularTotal())) {
            obtenerProvUnicos();
            String codigoPedido = "";
            for (Proveedor prov: listProv) {
                ArrayList<CopiaProducto> listCProd = agruparProd(prov);
                Pedido pedido = crearPedido(listCProd, prov);
                prov.aceptarPedido(pedido);
                cliente.aceptarPedido(pedido);
                codigoPedido += "\t-" + pedido.getCodigo() + "\n";
            }
            String msj = enviarDetalleCompra();
            Mail mail = new Mail();
            mail.sendMail(this.cliente.getCorreo(), "Compra en Sistema Agricola", "Codigo de los Pedidos" 
                    + codigoPedido + "\n" + msj);    
        } else {
            System.out.println("No puede realizar el pago de sus productos");
        }
    }
    
    /**
     * Metodo que presenta por pantalla
     * el metodo enviarDetalleCompra() 
     */
    public void mostrarDetalleCompra () {
        System.out.println(enviarDetalleCompra());;
    }
    
    /**
     * Metod que retorna el mensaje
     * con los datos de la compra
     * @return mensaje
     */
    public String enviarDetalleCompra(){
        String msj;
        msj = "!----------Compra Realizada----------!\n";
        msj += String.format("%24s", "Productos\n");
        for (CopiaProducto cProd: this.listProd) {
            msj += cProd.consultaProdCompra() + "\n";
        }
        msj += "Total de la Compra: " + total;
        return msj; 
    }
    
    /**
     * Metodo que asigna un pedido comprado a su respectivo proveedor
     * (para que este lo procese) 
     * @param pedido 
     */
    public void asignarPedido (Pedido pedido) {
        Proveedor prov = pedido.getProveedor();
        prov.aceptarPedido(pedido);
    }
    
    /**
     * Metodo que obtiene un unico Proveedor
     */
    public void obtenerProvUnicos () {
        Proveedor prov;
        for (CopiaProducto cProd: listProd) {
            prov = cProd.getProducto().getProveedor();
            if (!listProv.contains(prov)) {
                listProv.add(prov);
            }
        }
    }
    
    /**
     * Metodo que obtiene el total de una lista de Productos
     * @return 
     */
    public double calcularTotal () {
        double suma = 0;
        for (CopiaProducto cProd: listProd) {
            suma += cProd.getCantidad() * cProd.getProducto().getPrecioUnitario();
        }
        return suma;
    }
    
    /**
     * Metodo que obtiene los productos
     * del Proveedor pasado como parametro
     * @param prov
     * @return 
     */
    public ArrayList<CopiaProducto> agruparProd (Proveedor prov) {
        ArrayList<CopiaProducto> listProdAgrup = new ArrayList<>();
        for (CopiaProducto cProd: listProd) {
            Proveedor condicionProv = cProd.getProducto().getProveedor();
            if (condicionProv.equals(prov)) {
                listProdAgrup.add(cProd);
            }
        }
        return listProdAgrup;
    }
    
    /**
     *  Metodo que crea un pedido
     * con los parametros
     * @param listProdAgrup-> Lista de productos por Proveedor
     * @param prov-> Proveedor
     * @return 
     */
    public Pedido crearPedido (ArrayList<CopiaProducto> listProdAgrup, Proveedor prov) {
        return new Pedido(listProdAgrup, cliente, this.pago, prov);
    }
    
    /**
     * Metodo que añade una CopiaProducto a
     * a lista de Productos de la clase
     * @param cProd 
     */
    public void añadirProducto (CopiaProducto cProd) {
        CopiaProducto cProdLista = hallarProducto(cProd.getProducto().getCodigo());
        if (cProdLista != null) {
            cProdLista.sumarExistencia(cProd.getCantidad());
        } else {
            this.listProd.add(cProd);
        }
    }
    
    /**
     * Metodo que elimina el Codigo
     * pasado como parametro
     * @param codigo 
     */
    public void eliminarProd (String codigo) {
        CopiaProducto cProd = hallarProducto(codigo);
        if (cProd != null) {
            listProd.remove(cProd);
            System.out.println("Producto eliminado exitosamente");
        } else {
            System.out.println("Producto no existe en Carrito");
        }
    }
    
    /**
     * Metodo que busca el Producto
     * con el codigo del mismo
     * @param codigo
     * @return 
     */
    private CopiaProducto hallarProducto(String codigo) {
        for (CopiaProducto cProd: listProd) {
            Producto prod = cProd.getProducto();
            if (prod.getCodigo().equals(codigo)) {
                return cProd;
            }
        }
        return null;
    }
    
    /**
     * Metodo que presenta por pantalla 
     * los productos que añadio el Cliente,
     * sino tiene ningun producto presenta un
     * mensaje por pantalla indicandole
     */
    public void consultarProds () {
        if (listProd.size()>0){
            for (CopiaProducto cProd: this.listProd) {
                System.out.println(cProd.consultaProdCompra());
            }
        }else{
            System.out.println("No tiene Productos");
        }
        
    }
    
    /**
     * Metodo que retorna una Lista de Producto
     * @return 
     */
    public ArrayList<Producto> getListProdCoincd() {
        return listProdCoincd;
    }
    
    /**
     * Metodo que retorna una Lista de Proveedores
     * @return 
     */
    public ArrayList<Proveedor> getListProvCoincd() {
        return listProvCoincd;
    }

    /**
     * Metodo retorna la Coordenada del Cliente
     * @return 
     */
    public Coordenada getUbicCliente() {
        return ubicCliente;
    }
    
    /**
     * Metodo que añade el Metodo de pago 
     * @param pago
     */
    public void setPago(MetodoPago pago) {
        this.pago = pago;
    }

    public ArrayList<CopiaProducto> getListProd() {
        return listProd;
    }

    public void setListProd(ArrayList<CopiaProducto> listProd) {
        this.listProd = listProd;
    }

    public ArrayList<Proveedor> getListProv() {
        return listProv;
    }

    public void setListProv(ArrayList<Proveedor> listProv) {
        this.listProv = listProv;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
}