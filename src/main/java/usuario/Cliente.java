/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import Sistema.CarritoCompra;
import Sistema.Coordenada;
import Sistema.MetodoPago;
import java.util.ArrayList;
import mercaderia.Pedido;

/**
 *Esta clase hereda los atributos de la clase Usuario,
 *nos permite definir metodos para comprar insumos alimenticios  
 * @author VictorGarcia
 */
public class Cliente extends Usuario{
    private ArrayList<MetodoPago> datosPago;
    private ArrayList<Pedido> listaPedido;
    private CarritoCompra carrito;
    
    public Cliente() {
        super();
    }
    
    /**
     * Constructor 2
     * @param usuario va a tener la informacion del Cliente 
     */
    public Cliente(Usuario usuario) {
        super(usuario.getNombre(), usuario.getDireccion(), usuario.getUbicacion(), 
              usuario.getCorreo(), usuario.getNombreUsuario(), usuario.getClave());
        datosPago = new ArrayList();
        listaPedido = new ArrayList();
        carrito = new CarritoCompra(this);
    }
    /**
     * Constructor1
     * nos va a permitir almacenar la informacion del Cliente 
     * @param nombre 
     * @param direccion
     * @param ubicacion -> Coordenadas gps
     * @param correo
     * @param nombreUsuario -> usuario con el que va a acceder al sistema
     * @param clave -> password del cliente 
     */
    public Cliente(String nombre, String direccion, Coordenada ubicacion, 
                   String correo, String nombreUsuario, String clave) {
        super(nombre, direccion, ubicacion, correo, nombreUsuario, clave);
        datosPago = new ArrayList();
        listaPedido = new ArrayList();
        carrito = new CarritoCompra(this);
    }
    
    /**
     * Metodo que va a añadir el Pedido a la lista de pedidos del Cliente
     * cuando este acepta la compra
     * @param pedido guarda la informacion de la compra(datos del cliente y los productos de compra)
     */
    public void aceptarPedido (Pedido pedido) {
        this.listaPedido.add(pedido);
    }
    
    /**
     * Metodo donde se obtiene el carrito de compras del Cliente
     * @return 
     */
    public CarritoCompra getCarrito() {
        return carrito;
    }

    public ArrayList<MetodoPago> getDatosPago() {
        return datosPago;
    }
    
    /**
     * Metdod donde se obtiene la informacion el metodo de pago
     * tarjeta de credito o paypal
     * @return 
     */
    
    @Override
    public String toString () {
        String mensaje = "Nombre: " + super.getNombre() + "\nDireccion: " + super.getDireccion() + 
                         "\nCorreo: " + super.getCorreo();
        return mensaje;
    }
    
    /**
     * 
     * @param pago 
     */
    public void agregarMetodoPago(MetodoPago pago){
        this.datosPago.add(pago);
    }
}