/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import Sistema.Coordenada;

/**
 * Esta clase padre abstracta nos va a permitir crear 2 tipos de clases usuarios
 *
 * @author VictorGarcia
 */
public class Usuario {
    private String nombre;
    private String nombreUsuario;
    private String clave;
    private String direccion;
    private Coordenada ubicacion;
    private String correo;
    
    public Usuario() {
        
    }
    /**
     * Constructor
     * donde los parametros ingresados 
     * van a ser los parametros de la clase
     * @param nombre
     * @param direccion
     * @param ubicacion
     * @param correo
     * @param nombreUsuario
     * @param clave 
     */
    public Usuario(String nombre, String direccion, Coordenada ubicacion, String correo, String nombreUsuario, String clave) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.ubicacion = ubicacion;
        this.correo = correo;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
    }
    
    
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }
    
    public String getDireccion() {
        return direccion;
    }

    public Coordenada getUbicacion() {
        return ubicacion;
    }

    public String getCorreo() {
        return correo;
    }
        
}
