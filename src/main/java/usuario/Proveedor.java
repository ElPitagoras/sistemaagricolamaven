/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package usuario;

import Sistema.Coordenada;
import java.time.LocalDate;
import java.util.ArrayList;
import mercaderia.Pedido;
import mercaderia.Producto;
import java.util.Date;
import mercaderia.*;

/**
 * Esta clase herada los atributos de la Clase Usuario Nos permite definir
 * metodos donde se puede vender productos de diferentes categorias,
 * modificarlos procesar pedidos
 *
 * @author Johnny Flores
 */
public class Proveedor extends Usuario{
    private String numContacto;
    private ArrayList <Producto> listProducto = new ArrayList();
    private ArrayList<Pedido> listPedido = new ArrayList();
    
    /**
     * Constructor1
     * almacena los datos del proveedor
     * @param nombre
     * @param direccion
     * @param ubicacion
     * @param correo
     * @param numContacto
     * @param nombreUsuario
     * @param clave 
     */
    public Proveedor(String nombre, String direccion, Coordenada ubicacion, 
                     String correo, String numContacto, String nombreUsuario, String clave) {
        super(nombre, direccion, ubicacion, correo, nombreUsuario, clave);
        this.numContacto = numContacto;
    }
    
    /**
     * Constructor2
     * almacena los datos del Proveedor
     * @param usuario contiene la informacion del proveedor
     * @param contacto 
     */
    public Proveedor(Usuario usuario, String contacto) {
        super(usuario.getNombre(), usuario.getDireccion(), usuario.getUbicacion(), usuario.getCorreo(), 
              usuario.getNombreUsuario(), usuario.getClave());
        this.numContacto = contacto;
    }

    public void setListProducto(ArrayList<Producto> listProducto) {
        this.listProducto = listProducto;
    }

    public ArrayList<Pedido> getListPedido() {
        return listPedido;
    }

    public String getNumContacto() {
        return numContacto;
    }

    public ArrayList<Producto> getListProducto() {
        return listProducto;
    }
    
    /**
     * Este metodo va a guardar el parametro Producto en la lista de productos que vende el Proveedor
     * @param prod -> Producto
     */
    public void registrarProducto(Producto prod){
        prod.setProveedor(this);
        listProducto.add(prod);                 
    }
    
    /**
     * Este metodo revisa la informacion de cada Pedido
     * codigo, fecha de compra, la lista de productos a entregar,
     * el estado del pedido (solicitado,procesando,despachado)
     */
    public void consultarPedidos(){
        for(Pedido pedido: listPedido ){
            System.out.println("\n");
            System.out.println(pedido);
        }
    }
    
    /**
     * Este metodo nos permite 
     * cambiar el estado del pedido (solicitado,procesando,despachado)
     * @param codigo --> el codigo del pedido que se va a cambiar del estado
     */
    public void cambiaEstadoPedido(String codigo){
        Pedido pedido = obtenerPedido(codigo);
        if (pedido != null) {
            EstadoPedido estado = pedido.getEstado();
            Date fecha = new Date();
            switch(estado){
                case Solicitado:
                    pedido.setEstado(EstadoPedido.Procesando);
                    pedido.setFechaProcesando(fecha);
                    System.out.println("El estado del pedido se cambio a: " + pedido.getEstado());
                    break;
                case Procesando:
                    pedido.setEstado(EstadoPedido.Despachado);
                    pedido.setFechaDespachado(fecha);
                    System.out.println("El estado del pedido se cambio a: " + pedido.getEstado());
                    break;
            }
        } else {
            System.out.println("Codigo de pedido no existe");
        }
    }
    
    /**
     * Este metodo va a permitir visualizar por pantala
     * los productos del Proveedor, 
     * segun los parametros(pueden estar vacios) 
     * @param categoria
     * @param nombre 
     */
    public void consultarProducto(ArrayList<Categoria> categoria, String nombre){
        ArrayList<Producto> listProdMatched = (ArrayList<Producto>) listProducto.clone();
        if (categoria != null) {
            for (Producto prod: this.listProducto) {
                if (!categoria.containsAll(prod.getCategoria())) {
                    listProdMatched.remove(prod);
                }
            }
        }
        if (!nombre.equals("")) {
            for (Producto prod: this.listProducto) {
                if (!nombre.equals(prod.getNombre())){
                    listProdMatched.remove(prod);
                }
            }
        }
        if (listProdMatched.size()>0) {
            for (Producto prod: listProdMatched) {
            System.out.println(prod.consultaProdUsuario());
            }
        } else {
            System.out.println("Aún no registra producto con esas condiciones");
        }
        
    }
    
    /**
     * Este metodo va a permitir modificar 
     * el producto mediante su codigo
     * los parametros pasados en el metodo pueden estar vacios
     * @param codigo -> codigo del Producto a modificar
     * @param nombre -> nuevo nombre del producto
     * @param cat -> las nuevas categorias que va a tener el Producto
     * @param precio -> nuevo precio por unidad del Producto
     * @param descripcion ->nueva descripcion del Producto
     */
    public void editarProducto(String codigo, String nombre, ArrayList<Categoria> cat, double precio, String descripcion){
        Producto prod = obtenerProducto(codigo);
        if (prod != null) {
            if (nombre != null) {
            prod.setNombre(nombre);
            }
            if (cat != null) {
                prod.setCategoria(cat);
            }
            if (precio != -1) {
                prod.setPrecioUnitario(precio);
            }
            if (descripcion != null) {
                prod.setDescripcion(descripcion);
            }
            System.out.println("Edición exitosa");
            prod.consultaProdUsuario();
        } else {
            System.out.println("Codigo no existe");
        }
    }
    
    /**
     * Este metodo permite obtener el Producto
     * mediante el codigo de este
     * @param codigoProd
     * @return  
     */
    public Producto obtenerProducto(String codigoProd){
        for (Producto prod: this.listProducto){
            if (prod.getCodigo().equals(codigoProd)){
                return prod;
            }
        }
        return null;
    }
    
    /**
     * Este metodo permite obtener el Pedido
     * mediante el codigo del mismo
     * @param codigoPed
     * @return 
     */
    private Pedido obtenerPedido(String codigoPed){
        for (Pedido ped: this.listPedido){
            if (ped.getCodigo().equals(codigoPed)){
                return ped;
            }
        }
        return null;
    }
    
    /**
     * Este metodo va a guardar el Pedido realizado
     * en la lista de Pedidos del Proveedor
     * @param pedido 
     */
    public void aceptarPedido(Pedido pedido) {
        this.listPedido.add(pedido);
    }
}